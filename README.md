# Des nombres et des mots

Pour ce dernier TP, vous allez faire du TDD seul.

L'objectif est de réaliser un convertisseur de nombres en lettres.

Vous allez implémenter la classe suivante : 

``` java
public class Numeral {
    public Numeral (String number) {
        ...
    }

    public String toLetters () {
        ...
    }
}
```

## Initialisation

### configuration de maven

Si ça n'a pas été fait, voir le [TP0](https://gitlab.univ-lille.fr/2019-coa-tdd/tp0/)

### Projet

Comme pour le [TP0](https://gitlab.univ-lille.fr/2019-coa-tdd/tp0/), créez un projet gitlab `tp3-nom` et importez le dans votre éditeur préféré.

Maintenant vous avez tout ce qu'il faut pour commencer à travailler.


### Barème de relecture

Dans toute la suite attention de bien suivre la boucle TDD et de faire des commits à la fin de chaque étape, et des push à la fin de chaque boucle. Un git log du projet devrait ressembler à ça :


```
...
b28a944 Refactor tests
b28a944 Impl addition
8ab9f43 Add second test for addition
efd6601 Green bar for first addition
2530399 Add first test for addition
f9aa3f3 Bootstrap project

```

Nous apporterons une attention toute particulière à la concordance entre les messages de commits, les heures de commits et les contenues des commits.

Les règles de code propre à respecter sont toujours les mêmes :

* pas de méthode de plus de 10 lignes
* nommage correct des variables et des méthodes : on comprend à quoi elles servent à la lecture et il n'y a pas d'abréviation.
* pas plus de 3 indentations par méthode
* pas de classe de plus de 50 lignes
* pas de commentaire, on comprend le code à la première lecture
* pas de répétition (DRY)
* le code est le plus simple possible (KISS, YAGNI) : si je peux enlever du code sans casser les tests c'est que je n'ai pas fini ma simplification
* Pas de "magic numbers", des nombre ou des Strings qui ne sortent de nulle part.
* Max 2 paramètres par méthode
* Pas plus d'un point par ligne (cf. notation fluent)
* Toutes les règles que vous avez rencontrées depuis 2 ans.

Enfin, chaque push va alimenter sonar : https://sonar.tcweb.org/. Sonar est un robot capable de détecter des bugs, des vulnérabilités ainsi que la couverture de code et le pourcentage de code dupliqué. Dans le cadre du TP il est important d'obtenir A dans toutes les notes, 100% de couverture de code et 0% de code dupliqué.

Pour finir, il va sans dire que l'objectif est d'aller le plus loin possible dans le sujet.

## Les Unités

En initialisant notre objet Numeral avec une string d'un chiffre, il est possible d'obtenir la version écrite en lettres de ce chiffre commençant par une majuscule.

Par exemple :

| Entrée      | Sortie |
|-------------|--------|
| 1           | Un     |
| 7           | Sept   |


## Première dizaine

En initialisant notre objet Numeral avec une string de deux chiffres, il est possible d'obtenir la version écrite en lettres de ce chiffre commençant par une majuscule.

Par exemple :

| Entrée      | Sortie |
|-------------|--------|
| 10          | Dix    |
| 18          | Dix-huit |

## Seconde dizaine

En initialisant notre objet Numeral avec une string de deux chiffres, il est possible d'obtenir la version écrite en lettres de ce chiffre commençant par une majuscule.

Par exemple :

| Entrée      | Sortie |
|-------------|--------|
| 20          | Vingt    |
| 21          | Vingt et un |

## Autres dizaines

En initialisant notre objet Numeral avec une string de deux chiffres, il est possible d'obtenir la version écrite en lettres de ce chiffre commençant par une majuscule.

Par exemple :

| Entrée      | Sortie |
|-------------|--------|
| 39          | Trente-neuf        |
| 96          | Quatre-vingt-seize |

## Centaines

En initialisant notre objet Numeral avec une string de trois chiffres, il est possible d'obtenir la version écrite en lettres de ce chiffre commençant par une majuscule.

Par exemple :

| Entrée      | Sortie |
|-------------|--------|
| 100         | Cent               |
| 200         | Deux cents               |
| 375         | Trois cent soixante-quinze |

## Des euros

En initialisant notre objet Numeral avec une string décrivant un prix en euro, il est possible d'obtenir la version écrite en lettres de ce prix commençant par une majuscule.
En Français, le symbole monétaire se place après tous les chiffres séparés par une espace.

Par exemple :

| Entrée      | Sortie |
|-------------|--------|
| 1 €         | Un euro              |
| 845 €       | Huit cent quarante-cinq euros |

## Des milles

En initialisant notre objet Numeral avec une string, il est possible d'obtenir la version écrite en lettres de ce chiffre ou du prix commençant par une majuscule.

Par exemple :

| Entrée      | Sortie |
|-------------|--------|
| 1000        | Mille              |
| 4000        | Quatre mille             |
| 5668 €      | Cinq mille six cents soixante-huit euros |

# Plus d'unités

* Ajouter la gestion d'autres monnaie comme le dollar, la livre ou le yen.
* Ajouter la gestion des unités métriques (km, m, cm, ...)

# Des nombres de plus en plus grand

* Ajouter la gestion des nombres jusqu'au milliard.
* Ajouter la gestion des quintillions.

# Affichage en chiffres

Ajouter la méthode `public String toFigures()` qui retourne le nombre formaté en respectant les règles typographique françaises (regroupement des chiffres par 3 séparés par des espaces).

# L'opération inverse

Ajouter la méthode `public Numeral fromLetters(String number)` qui permet de parser un nombre écrit en lettre.

# Barème de correction

Comme une dictée, la copie démarre avec 20 points. Chaque erreur enlève des points.

Pour garder les 20 points, le programme doit au moins savoir convertire jusqu'à 999. À partir de la gestion des euros, ce sont donc des questions bonus.

Dans Sonar :
* -3 par bug ou vulnérabilité bloquante
* -2 par bug ou vulnérabilité critique
* -1 par bug ou vulnérabilité majeure
* -0.5 par bug ou vulnérabilité pour tous les autres types.
* -0.5 par pourcentage de code non couvert
* -0.5 par pourcentage de code dupliqué

L'ensemble du code doit respecter des règles que nous avons eu (SOLID, lisibilité, DRY, KISS, YAGNI, etc.) en fonction de ces règles, estimez la note du code que vous relisez.

Un test rouge : -1 point

Je vous invite à ajouter vos propres tests pour vérifier que le code respecte bien le besoin métier. Attention le but est bien d'évaluer la qualité du code et pas sa complétude en rapport à toutes les questions. Si vous avez des tests correspondant aux questions traitées par les étudiants que vous corrigez qui ne passent pas vous pouvez enlever des points.

Pour vérifier que la boucle TDD a bien été respectée, vous avez 2 outils à votre disposition :
* `git log --oneline --decorate --graph --all --format="%h %Cblue%ad% %C(auto) %s"` qui permet de lister tous les commits avec l'heure et le commentaire
* `for commit in $(git log --reverse --pretty=format:%H); do git checkout $commit; git show --stat $commit; read; done` qui rejoue la liste de tous les commit 1 à 1 en affichant quelques éléments. Pour passer d'un commit à l'autre, il faut sortir du git show et valider avec la touche entrée.
* Pour revenir au dernier commit `git checkout master`

À chaque non respect de la boucle TDD -1 point s'impose.

Merci de remplir un fichier `notes.md` à la racine du projet que vous corrigez avec le contenu suivant :
Ajouter la gestion des quintillions.


```markdown
| Correcteur        | Note /20 |
|-------------------|----------|
| Nom               | XX       |
| Nom               | XX       |

# Commentaires

## Nom1

* item

## Nom2

* item

```

Je vais moi aussi noter votre code, si je trouve des notes trop gentilles ou trop sévère c'est 0 aux 2 copies.
